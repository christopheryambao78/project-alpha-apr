from django.urls import path
from tasks.views import create_project, show_my_tasks

urlpatterns = [
    path("create/", create_project, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
]
